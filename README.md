# Welcome to the Tentacles Project
Tentacles is a static site generator. It is a python package (see the `tentacles` folder) which contains a build script that generates 3 applictions based on what it finds in the `content` folder.

This app began as a showcase of data structures and algorithms problems that I was working on. The goal was that the user could read about the solution, but also try different inputs to test my solutions out for themselves. Now I can write solutions in 3 different languages and they are all supported by Tentacles.

The app is **live** at https://tentacles.willowpowell.com

# Design and Flavour
I initially began using Material UI for this project, but I decided it would be more fun to do all of the CSS custom, and to make it something of an art project. The goal was a spooky retro theme, which I think I achieved nicely.

# Architecture

![Tentacles architecture diagram](/TentaclesArch.jpg)

# Project Structure
The project consists of a few pieces:
1. The `tentacles` python app, which contains scripts for managing and building the other parts.
2. React app, in the `front-end` folder. A good portion of this app is generated by `tentacles build`, based on the templates in `templates/front-end`.
3. The `api` folder contains a Go (gin) and Python (flask) api that serve the frontend, and allow algorithms to be written in those languages. Also generated by `tentacles build`.
5. The `content` folder, with data structures and algorithms implementations by Willow, which are the basis for the posts.

*Enjoy <3<3<3<3*

# Setting up for development

## Create virtual environment and install dependencies
Use Python 3.9
```bash
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ pip install -e . # This installs the tentacles app and lets you run the scripts.
```

There is only one `requirements.txt` file, shared between the tentacles package and the flask api, just to keep things simple.

## Setup `.env`s
Copy `.env.example` twice! You need `.dev.env` and `.prod.env`

Currently the python and go apps are served at `tentacles-py.fly.dev` and `tentacles.fly.dev` respectively.

## Run the build script
```bash
$ tentacles build
```

## Install front end dependencies
```bash
$ cd front-end
$ npm i
```

## Start the front end
```bash
$ cd front-end
$ npm start
```
This will run the build script automatically to make sure you're up to date and using the right `.env`

## Start the backends
```bash
# in virtual environment
$ cd api/py
$ flask run
# and in another tab
$ cd api/go
$ go mod tidy
$ go run main.go
```

# Production

## Frontend
```bash
$ cd front-end
$ npm run build # automatically runs `tentacles build` with `.prod.env`
# Test your build:
$ netlify deploy
# When you're feeling confident:
$ netlify deploy --prod
```

## Python API
```bash
$ tentacles build --prod
$ cd api/py
$ flyctl deploy
```

## Go API

```bash
$ tentacles build --prod
$ cd go-api
$ flyctl deploy
```

# Adding new posts.
```bash
tentacles new-post
```

Then add the solution to the generated folder, along with a `.md` file for the post.

## Don't forget to export
If you're writing a JavaScript solution, you need to `export default` your function.
