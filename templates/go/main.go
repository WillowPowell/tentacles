package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
)

func main() {
	r := gin.Default()
	r.SetTrustedProxies(nil)
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	r.Use(cors.New(config))
<3>GO_ROUTE_REGISTRATIONS</3>
	r.Run()
}

type BinaryTree struct {
	Val int `json:"val"`
	Left *BinaryTree `json:"left"`
	Right *BinaryTree `json:"right"`
}

type BinaryTreeRoot struct {
	Root *BinaryTree
}

<3>GO_ROUTES</3>
