export default async function apiCall(...inputs) {
    const response = await fetch(`<3>FULL_API_ROUTE</3>`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(<3>API_CALL_BODY</3>)
    });
    const responseJSON = await response.json()

    return responseJSON.answer
}
