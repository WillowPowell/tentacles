import React from 'react';

import solution from './<3>name</3>';

import {
  BigO,
  Hero,
  Markdown,
  Playground,
  SyntaxHighlighter,
} from 'components'

const txtVersion = `<3>TEXT_SOLUTION</3>`;
const story = `<3>MARKDOWN</3>`;
const hero = {
  title: `<3>title</3>`,
  prompt: `<3>prompt</3>`,
};
const bigO = <3>big_o</3>;
const signature = <3>signature</3>;

export default function Post() {

  return (
    <main className="main">
      <div className="post">
        <Hero {...hero}  />
        <Markdown>{story}</Markdown>
        <h2>Solution</h2>
        <SyntaxHighlighter language="<3>language</3>">{txtVersion}</SyntaxHighlighter>
        <BigO {...bigO} />
        <Playground solution={solution} signature={signature} />
      </div>
    </main>
  )
}
