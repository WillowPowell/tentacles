import React from 'react';
import { NavLink } from 'react-router-dom';

import './Nav.css';
import NavDrawer from './NavDrawer';


export default function Nav() {
  return (
    <NavDrawer>
      <div className="nav-container container-col">
        <NavLink to="/" className="submarines"><h2>TENTACLES</h2></NavLink>
        <3>NAV_ITEMS</3>
        <h3>other-fun-things</h3>
        <ul>
          <li>
            <NavLink to="/binary-tree-playground">
              Binary Tree Playground
            </NavLink>
          </li>
        </ul>
      </div>
    </NavDrawer>
  );
}
