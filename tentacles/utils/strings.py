import functools
import re
import readline

def snake_to_slug(snake):
    return snake.replace("_", "-")

def camel_to_slug(camel):
    arr = re.split("([A-Z])", camel)
    def joiner(acc, el):
        new_word = bool(re.search("[A-Z]", el))
        if not new_word:
            return acc + el
        return acc + "-" + el.lower()
    return functools.reduce(joiner, arr, "")

def function_name_to_slug(language, name):
    if language == "py":
        return snake_to_slug(name)
    if language in ("js", "go"):
        return camel_to_slug(name)

def slug_to_title(slug):
    return " ".join([
        word[0].upper() + word[1:]
        for word in slug.split("-")
    ])

def camel_to_pascal(camel):
    return camel[0].upper() + camel[1:]

def slug_to_snake(slug):
	return slug.replace("-", "_")

def rlinput(prompt, prefill=''):
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return input(prompt)
    finally:
        readline.set_startup_hook()
