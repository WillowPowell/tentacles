import click
import os

from tentacles.scripts.build import build
from tentacles.scripts.new_post import new_post

@click.group()
def cli():
    # need to be in root of project because build scripts have relative file path references.
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    os.chdir("..")

cli.add_command(build)
cli.add_command(new_post)
