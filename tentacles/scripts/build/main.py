import json
import os
import re
import shutil
import yaml

from dotenv import load_dotenv
import click

from tentacles.config import config
from tentacles.utils.build import populate_template

from .models import JavaScriptPostGenerator, PythonPostGenerator, GoPostGenerator

@click.command()
@click.option('--prod/--dev', default=False, help='Build using .prod.env; default .dev.env')
def build(prod, ):
	prepare(prod)

	posts = walk_source_folder(config.ALGORITHMS_FOLDER)

	# Snippets to populate as we work through the data.
	strings_to_build = {
		"frontend_routes": '',
		"imports": '',
		"nav_items": '',
		"python_routes": '',
		"go_routes": '',
		"go_route_registrations": '',
	}

	# Copy and populate the template
	for category in posts.keys():
		category_dest_folder = f'{config.POSTS_DEST}/{category}'
		os.makedirs(category_dest_folder)
		# Each category needs its own
		strings_to_build["category_nav"] = ''

		for post_title in posts[category]:
			source_path = f'{config.ALGORITHMS_FOLDER}/{category}/{post_title}/{post_title}'
			metadata = process_metadata(source_path)
			if metadata["language"] == "js":
				generator = JavaScriptPostGenerator(metadata, category, strings_to_build)
			if metadata["language"] == "py":
				generator = PythonPostGenerator(metadata, category, strings_to_build)
			if metadata["language"] == "go":
				generator = GoPostGenerator(metadata, category, strings_to_build)
			generator.process_post()

		strings_to_build["nav_items"] += populate_template(config.NAV_CATEGORY_SNIPPET, {'CATEGORY': category, 'POSTS': strings_to_build["category_nav"]}, write=False)

	# Populate routes and nav after all categories are processed.
	routes_dest = f'{config.DEST_FOLDER}/Routes.jsx'
	shutil.copyfile(config.ROUTES_TEMPLATE, routes_dest)
	populate_template(routes_dest, {'ROUTES': strings_to_build["frontend_routes"], 'IMPORTS': strings_to_build["imports"]})

	nav_dest = f'{config.DEST_FOLDER}/Nav/index.jsx'
	shutil.copyfile(config.NAV_TEMPLATE, nav_dest)
	populate_template(nav_dest, {'NAV_ITEMS': strings_to_build["nav_items"]})

	playground_implementation = {}
	with open(f'{config.DATA_STRUCTURES_SOURCE_FOLDER}/BinaryTree.js') as f:
		playground_implementation['IMPLEMENTATION'] = f.read()
	playground_dest = f'{config.DEST_FOLDER}/BinaryTreePlayground.jsx'
	shutil.copyfile(config.BT_PLAYGROUND_TEMPLATE, playground_dest)
	populate_template(playground_dest, playground_implementation)

	shutil.copyfile(config.FLASK_APP_TEMPLATE, config.FLASK_APP_DEST)
	populate_template(config.FLASK_APP_DEST, {'PYTHON_ROUTES': strings_to_build["python_routes"]}, comment_char="#")

	shutil.copyfile(config.GO_MAIN_TEMPLATE, config.GO_MAIN_DEST)
	populate_template(config.GO_MAIN_DEST, {
		'GO_ROUTES': strings_to_build["go_routes"],
		'GO_ROUTE_REGISTRATIONS': strings_to_build["go_route_registrations"]
	})


def walk_source_folder(source_folder):
	'''
	Every yml/md file should have a corresponding js file.
	Subcategories have not been tested.

	returns { category_1: [post_title_1, post_title_2, ...], ... }
	'''
	categories = config.exclude(os.listdir(source_folder))
	category_paths = map(lambda d: f"{source_folder}/{d}", categories)

	posts = {
		categories[i]: config.exclude(os.listdir(cat))
		for i, cat in enumerate(category_paths)
		if os.path.isdir(cat)
	}

	return posts


def process_metadata(source_path):
	with open(f'{source_path}.yml') as f:
		metadata = yaml.safe_load(f.read())

	try:
		with open(f'{source_path}.md') as f:
			markdown = f.read()
	except:
		markdown = ""

	# Add some special variables to the metadata.
	metadata['MARKDOWN'] = markdown
	with open(f'{source_path}.{metadata["language"]}') as f:
		metadata['TEXT_SOLUTION'] = f.read()
	metadata['COMPONENT_NAME'] = f'{metadata["title"].replace(" ", "")}Post'

	return metadata


def prepare(prod):
	env_file = '.prod.env' if prod else '.dev.env'

	if not os.path.exists(env_file):
		raise Exception(f"env file {env_file} does not exist")

	print(f"Building with {env_file}")
	load_dotenv(env_file)

	# Frontend
	if os.path.exists(config.POSTS_DEST):
		shutil.rmtree(config.POSTS_DEST)
	if os.path.exists(config.DATA_STRUCTURES_DEST):
		shutil.rmtree(config.DATA_STRUCTURES_DEST)
	shutil.copytree(config.DATA_STRUCTURES_SOURCE_FOLDER, config.DATA_STRUCTURES_DEST)

	# Python API
	if os.path.exists(config.FLASK_APP_DEST):
		os.remove(config.FLASK_APP_DEST)
	if os.path.exists(config.FLASK_APP_REQUIREMENTS_DEST):
		os.remove(config.FLASK_APP_REQUIREMENTS_DEST)
	shutil.copyfile(config.PYTHON_REQUIREMENTS, config.FLASK_APP_REQUIREMENTS_DEST)

	# GO API
	if os.path.exists(config.GO_MAIN_DEST):
		os.remove(config.GO_MAIN_DEST)
