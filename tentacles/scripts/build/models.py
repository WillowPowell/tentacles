import shutil
import os

from tentacles.config import config
from tentacles.utils.strings import camel_to_pascal
from tentacles.utils.build import populate_template

class PostGenerator:
    def __init__(self, metadata, category, strings_to_build):
        self.dest_folder = f'{config.POSTS_DEST}/{category}'
        self.metadata = metadata
        self.category = category
        self.strings_to_build = strings_to_build
        self.metadata['CATEGORY'] = self.category
        self.source_path = f'{config.ALGORITHMS_FOLDER}/{self.category}/{self.metadata["name"]}/{self.metadata["name"]}'

    def language_specific_processing(self):
        pass

    def process_post(self):
        # Copy and populate the Post template
        post_component = f'{self.dest_folder}/{self.metadata["COMPONENT_NAME"]}.jsx'
        shutil.copyfile(config.POST_TEMPLATE, post_component)
        populate_template(post_component, self.metadata)

        self.language_specific_processing()

        # Extend routes and imports
        self.strings_to_build["frontend_routes"] += populate_template(config.ROUTE_SNIPPET, self.metadata, write=False)
        self.strings_to_build["imports"] += populate_template(config.IMPORT_SNIPPET, self.metadata, write=False)
        self.strings_to_build["category_nav"] += populate_template(config.NAV_ITEM_SNIPPET, self.metadata, write=False)

class JavaScriptPostGenerator(PostGenerator):
    def language_specific_processing(self):
        shutil.copyfile(f'{self.source_path}.js', f'{self.dest_folder}/{self.metadata["name"]}.js')

class NonJavaScriptPostGenerator(PostGenerator):
    api_route_env_var = ""
    def make_api_call_body(self):
        return "inputs"
    def language_specific_processing(self):
        self.metadata['API_ROUTE_BASE'] = f"/api/{self.metadata['language']}/{self.category}/{self.metadata['slug']}"
        self.metadata['FULL_API_ROUTE'] = os.getenv(self.api_route_env_var) + self.metadata['API_ROUTE_BASE']
        self.metadata['API_CALL_BODY'] = self.make_api_call_body()
        api_call = f'{self.dest_folder}/{self.metadata["name"]}.js'
        shutil.copyfile(config.API_CALL_TEMPLATE, api_call)
        populate_template(api_call, self.metadata)

class PythonPostGenerator(NonJavaScriptPostGenerator):
    api_route_env_var = "REACT_APP_PYTHON_API_URL"
    def language_specific_processing(self):
        super().language_specific_processing()
        self.metadata['SOLUTION'] = ""
        with open(f'{self.source_path}.py') as f:
            for line in f.readlines():
                self.metadata['SOLUTION'] += f"	{line}"
        self.strings_to_build["python_routes"] += populate_template(config.FLASK_ROUTE_SNIPPET, self.metadata, write=False)

class GoPostGenerator(NonJavaScriptPostGenerator):
    # GO cannot support a regular BinaryTree that might be null...
    type_map = {
        "Array": "[]",
        "Number": "int",
        "String": "string",
        "BinaryTree": "BinaryTreeRoot"
    }
    api_route_env_var = "REACT_APP_GO_API_URL"
    def language_specific_processing(self):
        super().language_specific_processing()
        self.metadata["PASCAL_NAME"] = camel_to_pascal(self.metadata["name"])
        self.metadata["REQUEST_STRUCT_ARGS"] = """
        """.join([
            f"{camel_to_pascal(arg['name'])}  {self.type_map[arg['type']]}"
            for arg in self.metadata["signature"]["inputs"]
        ])
        self.metadata["FUNCTION_CALL_ARGS"] = ", ".join([
            f"request.{camel_to_pascal(arg['name'])}{'.Root' if arg['type'] == 'BinaryTree' else ''}"
            for arg in self.metadata["signature"]["inputs"]
        ])

        self.metadata['SOLUTION'] = ""
        with open(f'{self.source_path}.go') as f:
            for line in f.readlines():
                self.metadata['SOLUTION'] += f"	{line}"

        self.strings_to_build["go_routes"] += populate_template(config.GO_ROUTE_SNIPPET, self.metadata, write=False)
        self.strings_to_build["go_route_registrations"] += populate_template(config.GO_ROUTE_REGISTRATION_SNIPPET, self.metadata, write=False)

    def make_api_call_body(self):
        # Go needs named keys to satisfy strong typing
        return "{" + f", ".join([
            f"{camel_to_pascal(arg['name'])}: {'{ Root: ' if arg['type'] == 'BinaryTree' else ''} inputs[{i}] {'}' if arg['type'] == 'BinaryTree' else ''}"
            for i, arg in enumerate(self.metadata["signature"]["inputs"])
        ]) + "}"
