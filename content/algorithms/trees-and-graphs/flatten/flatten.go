func flatten(root *BinaryTree) *BinaryTree {
	for cur := root; cur != nil; cur = cur.Right {
		if cur.Left != nil {
			thread(cur)
		}
	}
	return root
}

func thread(node *BinaryTree) {
	cur := node.Left
	for cur.Right != nil {
		cur = cur.Right
	}
	cur.Right = node.Right
	node.Right = node.Left
	node.Left = nil
}
