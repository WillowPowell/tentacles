## Basic strategy
If we perform an inorder traversal, we can easily check where "broken" nodes are, by checking nodes against their neighbours. If we keep track of the bad nodes we find, the two that should be swapped will always be the first bad node and the last bad node we find.

## The real challenge
Usually to do an inorder traversal, we'd either use recursion or a stack to visit all of the nodes. Both of those solutions require `O(n)` space though!

This was actually a team effort. One of the other members of my data structres and algorithms club found the "Morris Traversal", and shared [this article](https://levelup.gitconnected.com/morris-traversal-for-binary-trees-e36e43a665cf) with an implementation in Java.

## Morris Traversal
This is a really cool algorithm, using a "threaded" binary tree. The tree is not threaded when you get it, but you add threads as you go. The basic approach is:
1. Check if the node has a left child. If it doesn't, visit it and go right. If there's no right child, you're done.
2. If there's a left child, get the "previous" node, meaning the one that comes before it in an inorder traversal. This is the rightmost child of the left child. "Thread" the tree by pointing the previous node to the node you're processing. Then go left.

So all you're doing is finding empty right nodes and changing thir pointers!

## The `getPrev` function
This function gets the "previous" node defined above, but it makes sure that it doesn't return the node that was passed in.

Then after you call `getPrev` you can check if the returned node has a right node. If it does, that means the node you're processing has already been threaded. It's time to break the thread, process that node, and go right.
