## The goal
If we use a standard Binary Tree traversal, we should be able to do this with `On` time complexity according to the [Master Theorem application to common algorithms](https://en.wikipedia.org/wiki/Master_theorem_%28analysis_of_algorithms%29#Application_to_common_algorithms).
## Naive Solution
My initial solution looked like this:

```js
function minimalTree(arr) {
  if (arr.length === 0) {
    return null;
  }
  if (arr.length === 1) {
    return new BinaryTree(arr[0], null, null);
  }
  const centreIndex = Math.floor(arr.length / 2);
  const left = minimalTree(arr.slice(0, centreIndex));
  const right = minimalTree(arr.slice(centreIndex + 1));
  return new BinaryTree(arr[centreIndex], left, right);
}
```

But there's a problem here. `Array.prototype.slice` is [not a constant time operation](https://stackoverflow.com/a/22615787/15038439). It is `On`, where `n` is the start and end indices. In order for our Binary Tree Traversal to fit with the Master Theorem application, it needs to be doing constant time operations in each recursive call.

But since ours is doing a linear operation each time, the time complexity looks more like [Merge Sort](https://en.wikipedia.org/wiki/Merge_sort#/media/File:Merge_sort_algorithm_diagram.svg). To unpack with our example, the calls to `minimalTree` at each level of recursion look like this.

`arr.length` is our `n`: `8` in this example:

```js
[-23, 0, 35, 44, 100, 101, 165, 200] // 8 operations
[-23, 0, 35] [100, 101, 165, 200] // 7 operations
[-23] [35] [100] [165, 200] // 5 operations
[200] // 1 operation
```
Instead of our target `n`, we're getting something like `n/2 * log N`, which is `O(n log n)`.

## Better Solution
The only reason we are getting `O n log n` results is because of `arr.slice` and its pesky `On` time complexity. How can we implement a similar algorithm without `arr.slice`?

Instead of manipulating the array, we can pass the *indices* of the array to process without `slice`ing it. We create an inner function to do the heavy lifting, and return the result of calling that function with the outside bounds of the array.
