## Naive solution
The "naive" approach to this problem, is quite simple:

```py
def rec_mult(a,b):
  if b == 0:
    return 0
  if b == 1:
    return a
  return a + rec_mult(a, b-1)
```

The time complexity of this would be `O(b)` - not great.

## Bit manipulation, anyone?
The question mentioned bit manipulation, so I thought that would be a good place to start.

After some research and experimentation, I discovered you can effectively multiply a number by a power of two by shifting left by whatever the power of two is. For example, 3 << 4 is the same as 3 * 2^4.

This is a significantly more efficient way to do it. Instead of only reducing `b` by 1 on each recursion, we reduce `b` by its most significant bit!
